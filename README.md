# Popular Movies

Name: Luis Fernandez Martinez

## Approach
When launch, the app will do the following: 

* Fetches automatically the first page of the popular movies.
* When scrolling down it will load the next page.
* If there is an error fetching the next page, a retry button will be displayed for that purpose at the bottom of the list.


## Architecture
I've used a `MVVM` approach:

* `MainActivity` will display the list of the movies received in a `RecyclerView`. When the user reaches the bottom of the list (page) will display a `loading` item and sends a `Commands` via `PublishSubject` to the `ViewModel` to fetch the next page.
* `MainActivityViewModel` will keep the state using Rx `scan` acting as an accumulator. When processing the `Commands` will report back to `MainActivity` using `LiveData`. Will delegate the requests to the `MovieRepository`.
* `MovieRepository` does the requests and passes back the result as a `Single`. Uses the arrow type `Either` to model `success` or `error`.
* All this is glued with `Koin` since I've seen that Curve android app uses it.

## Improvements
With more time I would have implemented a proper `Repository` approach with an interface in case we want to cache the results and a `UseCase`.
Also the movie api has a `configuration` endpoint with the available image sizes - this should be fetched and cached during the app launch and used
 later on when loading the images for each movie. For the purpose of the exercise I'm, using size `w500`.
 
## Tests
I've included unit tests and UI tests done with `espresso`.

I've tested the App and run tests with emulator api 28. The UI tests are a bit flaky, so probably a custom `IdlingResource` or a `ViewMatcher` with extra retries is needed.
