package com.crazyfriday.popularmovies

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.KoinComponent
import org.koin.core.inject


@RunWith(AndroidJUnit4::class)
class MainActivityTest : KoinComponent {

    val okHttp by inject<OkHttpClient>()
    private val mockWebServer = MockWebServer()
    private val portNumber = 4567

    @Rule
    @JvmField
    val rule = ActivityTestRule(MainActivity::class.java, true, false)


    @Rule
    @JvmField
    val idlingRule = CustomOkHttpIdlingResourceRule(okHttp)

    @Before
    fun setup() {
        mockWebServer.start(portNumber)
        mockWebServer.url("127.0.0.1").toString()
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
    }

    @Test
    fun when_loading_first_page_and_error_then_display_retry_button() {
        mockWebServer.enqueue(MockResponse().setBody("{ 'status': 'KO' }"))

        rule.launchActivity(null)
        onView(withText(R.string.try_again)).check(matches(isDisplayed()))
            .check(matches(isClickable()))
    }

    @Test
    fun when_loading_first_page_and_success_then_display_results() {
        mockWebServer.enqueue(MockResponse().setBody(String(response())))
        rule.launchActivity(null)
        onView(withId(R.id.movieList)).check(matches(isDisplayed()))

    }

    private fun response(fileName: String = "success.json") =
        InstrumentationRegistry.getInstrumentation().context.assets.open(fileName).readBytes()
}