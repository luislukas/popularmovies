package com.crazyfriday.popularmovies

import android.app.Application
import com.crazyfriday.popularmovies.di.provideMovieApi
import com.crazyfriday.popularmovies.di.provideMovieRepository
import com.crazyfriday.popularmovies.di.provideOkHttpClient
import com.crazyfriday.popularmovies.di.viewModelModule
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class KoinTestApp: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@KoinTestApp)
            modules(listOf(testNetworkModule, viewModelModule))
        }
    }

}
val testNetworkModule = module {
    single { testProvideRetrofit(get()) }
    single { provideOkHttpClient() }
    single { provideMovieApi(get()) }
    factory { provideMovieRepository(get()) }
}

fun testProvideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl("http://127.0.0.1:4567/").client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}