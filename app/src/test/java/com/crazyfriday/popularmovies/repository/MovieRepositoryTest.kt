package com.crazyfriday.popularmovies.repository

import arrow.core.Either.Left
import arrow.core.Either.Right
import com.crazyfriday.popularmovies.api.MovieApi
import com.crazyfriday.popularmovies.model.MovieListResponse
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Test

class MovieRepositoryTest {

    private val api = mock<MovieApi>()
    private val mainThreadScheduler = Schedulers.trampoline()
    private val ioThreadScheduler = Schedulers.trampoline()
    private val repository = MovieRepository(api, ioThreadScheduler, mainThreadScheduler)

    @Test
    fun `when there is an error then error result is returned`() {
        val movieList = mock<MovieListResponse>()

        whenever(api.list(1)).thenReturn(Single.just(movieList))
        whenever(movieList.success).thenReturn(false)

        repository.list().test().assertValue {
            (it as Left).a == MovieResultError
        }
    }

    @Test
    fun `when there is an exception then error result is returned`() {
        whenever(api.list(1)).thenReturn(Single.error(Throwable()))

        repository.list().test().assertValue {
            (it as Left).a == MovieResultError
        }
    }

    @Test
    fun `when there is an success then return result`() {
        val movieList = mock<MovieListResponse>()
        val expectedResult = MovieResultSuccess(movieList)

        whenever(api.list(1)).thenReturn(Single.just(movieList))
        whenever(movieList.success).thenReturn(true)

        repository.list().test().assertValue {
            (it as Right).b == expectedResult
        }

        verify(api).list(1)
    }


}