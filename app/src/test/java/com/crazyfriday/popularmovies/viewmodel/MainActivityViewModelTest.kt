package com.crazyfriday.popularmovies.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import arrow.core.Either
import arrow.core.left
import arrow.core.right
import com.crazyfriday.popularmovies.RxImmediateSchedulerRule
import com.crazyfriday.popularmovies.model.MovieListResponse
import com.crazyfriday.popularmovies.model.MovieResult.Movie
import com.crazyfriday.popularmovies.repository.MovieRepository
import com.crazyfriday.popularmovies.repository.MovieResultError
import com.crazyfriday.popularmovies.repository.MovieResultSuccess
import com.crazyfriday.popularmovies.viewmodel.MainActivityViewModel.*
import com.crazyfriday.popularmovies.viewmodel.MainActivityViewModel.Commands.OnLoad
import com.crazyfriday.popularmovies.viewmodel.MainActivityViewModel.Commands.OnLoadNextPage
import com.crazyfriday.popularmovies.viewmodel.MainActivityViewModel.MovieListState.*
import com.crazyfriday.popularmovies.viewmodel.MainActivityViewModel.Target
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.hasItems
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MainActivityViewModelTest {

    private val repository = mock<MovieRepository>()
    private val movieList = mock<MovieListResponse>()
    private val movies = mock<List<Movie>>()

    private val resultErrorResponse = MovieResultError.left()
    private val resultSuccessResponse = MovieResultSuccess(movieList).right()

    private val successResponse =
        Single.just<Either<MovieResultError, MovieResultSuccess>>(resultSuccessResponse)
    private val errorResponse =
        Single.just<Either<MovieResultError, MovieResultSuccess>>(resultErrorResponse)

    private val viewModel = MainActivityViewModel(repository)

    private val view = MockTestView()

    private val testObserver = TestObserver<MovieListState>()

    private val initialState: MovieListState = InitialState(ViewModelState())

    @Rule
    @JvmField
    var testSchedulerRule =
        RxImmediateSchedulerRule()

    @Rule
    @JvmField
    val ruleForLivaData = InstantTaskExecutorRule()

    @Before
    fun setup() {
        whenever(movieList.results).thenReturn(movies)
        viewModel.data.observeForever(testObserver)
    }

    @Test
    fun `when view is attached initial state is returned`() {
        viewModel.onAttach(view)

        assertThat(testObserver.observedValues.size, `is`(1))
        assertThat(testObserver.observedValues[0], `is`(initialState))
    }

    @Test
    fun `when there is an error and is first page then error state is returned`() {
        val loadingState: MovieListState = OnLoadState(initialState.state)
        val expectedResult: MovieListState = ErrorState(initialState.state)
        whenever(repository.list()).thenReturn(errorResponse)

        viewModel.onAttach(view)
        view.viewEvent.onNext(OnLoad)

        assertThat(testObserver.observedValues.size, `is`(3))
        assertThat(testObserver.observedValues, hasItems(loadingState, expectedResult))
    }

    @Test
    fun `when there is an error on third page then error state is returned`() {
        val expectedSuccessResult: MovieListState =
            SuccessState(
                initialState.state.copy(
                    movieList = movies,
                    hasMorePages = true,
                    page = 3
                )
            )

        val expectedErrorResult: MovieListState =
            ErrorStateLoadingPage(
                initialState.state.copy(
                    hasMorePages = true,
                    page = 2
                )
            )

        whenever(repository.list(any()))
            .thenReturn(successResponse)
            .thenReturn(errorResponse)

        whenever(movieList.page).thenReturn(3)

        viewModel.onAttach(view)
        view.viewEvent.onNext(OnLoadNextPage)
        view.viewEvent.onNext(OnLoadNextPage)


        assertThat(testObserver.observedValues.size, `is`(5))
        assertThat(
            testObserver.observedValues,
            hasItems(expectedSuccessResult, expectedErrorResult)
        )
    }

    @Test
    fun `when loading first page successfully then returns the list `() {
        val loadingState: MovieListState = OnLoadState(initialState.state)

        val expectedResult: MovieListState =
            SuccessState(initialState.state.copy(movieList = movies, hasMorePages = true, page = 2))

        whenever(repository.list()).thenReturn(successResponse)
        whenever(movieList.page).thenReturn(2)

        viewModel.onAttach(view)
        view.viewEvent.onNext(OnLoad)


        assertThat(testObserver.observedValues.size, `is`(3))
        assertThat(testObserver.observedValues, hasItems(loadingState, expectedResult))
    }

    @Test
    fun `when loading next page successfully then return new list `() {
        val loadingState: MovieListState =
            OnLoadState(initialState.state.copy(page = initialState.state.page + 1))

        val expectedResult: MovieListState =
            SuccessState(initialState.state.copy(movieList = movies, hasMorePages = true, page = 3))

        whenever(repository.list(2)).thenReturn(successResponse)
        whenever(movieList.page).thenReturn(3)

        viewModel.onAttach(view)
        view.viewEvent.onNext(OnLoadNextPage)


        assertThat(testObserver.observedValues.size, `is`(3))
        assertThat(testObserver.observedValues, hasItems(loadingState, expectedResult))
    }

    private class MockTestView : Target {
        override val viewEvent = PublishSubject.create<Commands>()
    }

    private class TestObserver<T> : Observer<T> {

        val observedValues = mutableListOf<T?>()

        override fun onChanged(value: T?) {
            observedValues.add(value)
        }
    }

}