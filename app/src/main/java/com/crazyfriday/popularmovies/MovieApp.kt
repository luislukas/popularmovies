package com.crazyfriday.popularmovies

import android.app.Application
import com.crazyfriday.popularmovies.di.networkModule
import com.crazyfriday.popularmovies.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MovieApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MovieApp)
            modules(listOf(networkModule, viewModelModule))
        }
    }
}