package com.crazyfriday.popularmovies.adapter

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.crazyfriday.popularmovies.BuildConfig
import com.crazyfriday.popularmovies.R
import com.crazyfriday.popularmovies.adapter.MovieListAdapter.ViewHolderType.*
import com.crazyfriday.popularmovies.model.MovieResult
import com.crazyfriday.popularmovies.model.MovieResult.*
import com.crazyfriday.popularmovies.viewmodel.MainActivityViewModel
import com.crazyfriday.popularmovies.viewmodel.MainActivityViewModel.Commands.OnLoadNextPage
import io.reactivex.subjects.PublishSubject
import kotlin.LazyThreadSafetyMode.NONE

class MovieListAdapter(
    private val subject: PublishSubject<MainActivityViewModel.Commands>,
    private val layoutInflater: LayoutInflater
) : RecyclerView.Adapter<ViewHolder>() {

    private val movieList: MutableList<MovieResult> = mutableListOf()

    fun setMovies(list: List<Movie>, morePages: Boolean) {
        if (movieList.size > 0) {
            movieList.removeAt(movieList.size - 1)
        }

        movieList.addAll(list)

        if (morePages) {
            movieList.add(LoadingItem)
        }

        notifyDataSetChanged()
    }

    fun errorLoading() {
        movieList.removeAt(movieList.size -1)
        movieList.add(ErrorLoadingItem)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (values()[viewType]) {
            MOVIE -> {
                CustomViewHolder(
                    layoutInflater.inflate(R.layout.view_movie, parent, false)
                )
            }
            LOADING -> {
                LoadingViewHolder(
                    layoutInflater.inflate(R.layout.view_progress, parent, false)
                )
            }
            ERROR_LOADING -> {
                ErrorLoadingViewHolder(
                    layoutInflater.inflate(R.layout.view_reload, parent, false)
                ) {
                    subject.onNext(MainActivityViewModel.Commands.OnLoad)
                }
            }
        }
    }

    override fun getItemCount(): Int = movieList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = movieList[position]
        when {
            holder is CustomViewHolder && item is Movie -> holder.bind(item)
            holder is LoadingViewHolder && item is LoadingItem -> subject.onNext(OnLoadNextPage)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (movieList[position]) {
            is Movie -> MOVIE
            is LoadingItem -> LOADING
            is ErrorLoadingItem -> ERROR_LOADING
        }.ordinal
    }

    private class CustomViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        private val title by lazy<TextView>(NONE) { view.findViewById(R.id.title) }
        private val voting by lazy<TextView>(NONE) { view.findViewById(R.id.voting) }
        private val movieImage by lazy<ImageView>(NONE) { view.findViewById(R.id.movieImage) }
        private val releaseDate by lazy<TextView>(NONE) { view.findViewById(R.id.releaseDate) }
        private val background by lazy<View>(NONE) { view.findViewById(R.id.background) }
        private val resources by lazy<Resources> { view.context.resources }

        fun bind(movie: Movie) {
            title.text = movie.title
            movieImage.contentDescription = movie.title

            val release = String.format(resources.getString(R.string.release_date), movie.releaseDate)
            releaseDate.text = release

            val votingAverage = String.format(
                resources.getString(R.string.people_liked),
                (movie.votingAverage * 10).toInt()
            )
            voting.text = votingAverage

            background.contentDescription = "${movie.title} $votingAverage $release"

            Glide.with(view.context)
                .load("${BuildConfig.BASE_POSTER_URL}${movie.imageUrl}")
                .centerInside()
                .placeholder(R.drawable.poster_placeholder)
                .into(movieImage)
        }
    }

    private class LoadingViewHolder(view: View) : RecyclerView.ViewHolder(view)
    private class ErrorLoadingViewHolder(view: View, onClick: () -> Unit) : RecyclerView.ViewHolder(view) {
        init {
            view.findViewById<Button>(R.id.reload).setOnClickListener {
                it.visibility = View.GONE
                view.findViewById<ProgressBar>(R.id.progress).visibility = View.VISIBLE
                onClick.invoke()
            }
        }
    }

    private enum class ViewHolderType {
        MOVIE, LOADING, ERROR_LOADING
    }

}