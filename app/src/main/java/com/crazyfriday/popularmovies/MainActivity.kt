package com.crazyfriday.popularmovies

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.crazyfriday.popularmovies.adapter.MovieListAdapter
import com.crazyfriday.popularmovies.viewmodel.MainActivityViewModel
import com.crazyfriday.popularmovies.viewmodel.MainActivityViewModel.Commands.OnLoad
import com.crazyfriday.popularmovies.viewmodel.MainActivityViewModel.MovieListState.ErrorState
import com.crazyfriday.popularmovies.viewmodel.MainActivityViewModel.MovieListState.ErrorStateLoadingPage
import com.crazyfriday.popularmovies.viewmodel.MainActivityViewModel.MovieListState.InitialState
import com.crazyfriday.popularmovies.viewmodel.MainActivityViewModel.MovieListState.SuccessState
import io.reactivex.subjects.PublishSubject
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.LazyThreadSafetyMode.NONE

class MainActivity : AppCompatActivity(), MainActivityViewModel.Target {

    private val viewModel: MainActivityViewModel by viewModel()

    override val viewEvent = PublishSubject.create<MainActivityViewModel.Commands>()

    private val list by lazy(NONE) { findViewById<RecyclerView>(R.id.movieList) }
    private val progress by lazy(NONE) { findViewById<ProgressBar>(R.id.progress) }
    private val errorButton by lazy(NONE) { findViewById<Button>(R.id.error) }

    private lateinit var listAdapter: MovieListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        with(viewModel) {
            data.observe(this@MainActivity, Observer(::handleState))
            onAttach(this@MainActivity)
        }

        with(list) {
            setHasFixedSize(true)
            adapter = MovieListAdapter(viewEvent, LayoutInflater.from(context)).also {
                listAdapter = it
            }
        }
        errorButton.setOnClickListener {
            viewEvent.onNext(OnLoad)
        }
        viewEvent.onNext(OnLoad)
    }

    override fun onDestroy() {
        viewModel.onDetach()
        super.onDestroy()
    }

    private fun handleState(state: MainActivityViewModel.MovieListState) {
        Log.d(MainActivity::class.java.simpleName, "HandleState $state")
        when (state) {
            is InitialState -> {
                list.visibility = View.GONE
                errorButton.visibility = View.GONE
                progress.visibility = View.VISIBLE
            }
            is ErrorState -> {
                list.visibility = View.GONE
                progress.visibility = View.GONE
                errorButton.visibility = View.VISIBLE
            }
            is ErrorStateLoadingPage -> listAdapter.errorLoading()
            is SuccessState -> {
                progress.visibility = View.GONE
                errorButton.visibility = View.GONE
                list.visibility = View.VISIBLE
                listAdapter.setMovies(state.state.movieList, state.state.hasMorePages)
            }
        }
    }
}
