package com.crazyfriday.popularmovies.api

import com.crazyfriday.popularmovies.BuildConfig
import com.crazyfriday.popularmovies.model.MovieListResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieApi {
    @GET("/3/movie/popular?api_key=${BuildConfig.API_KEY}")
    fun list(@Query("page") page: Int): Single<MovieListResponse>
}