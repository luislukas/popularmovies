package com.crazyfriday.popularmovies.repository

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import com.crazyfriday.popularmovies.api.MovieApi
import com.crazyfriday.popularmovies.model.MovieListResponse
import io.reactivex.Scheduler
import io.reactivex.Single

typealias MovieResult = Either<MovieResultError, MovieResultSuccess>

object MovieResultError
data class MovieResultSuccess(val list: MovieListResponse)

class MovieRepository(
    private val api: MovieApi,
    private val ioScheduler: Scheduler,
    private val mainThreadScheduler: Scheduler
) {

    fun list(page: Int = 1): Single<MovieResult> {
        return api.list(page)
            .subscribeOn(ioScheduler)
            .observeOn(mainThreadScheduler)
            .onErrorReturn { MovieListResponse(success = false) }
            .map { t ->
                if (t.success) {
                    MovieResultSuccess(t).right()
                } else {
                    MovieResultError.left()
                }
            }
    }
}