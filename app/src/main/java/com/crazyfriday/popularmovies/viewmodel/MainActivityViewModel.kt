package com.crazyfriday.popularmovies.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.crazyfriday.popularmovies.model.MovieResult.Movie
import com.crazyfriday.popularmovies.repository.MovieRepository
import com.crazyfriday.popularmovies.viewmodel.MainActivityViewModel.Commands.OnLoadNextPage
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

class MainActivityViewModel(private val repository: MovieRepository) : ViewModel() {

    private val initialState: MovieListState = MovieListState.InitialState(state = ViewModelState())
    private val composite = CompositeDisposable()
    val data = MutableLiveData<MovieListState>()

    fun onAttach(view: Target) {
        val observable = view.viewEvent.scan(initialState) { oldState, event ->
            Log.d(MainActivityViewModel::class.java.simpleName, "HandleState $event")
            when (event) {
                is Commands.OnLoad -> {
                    repository.list(initialState.state.page).subscribe { result ->
                        result.fold({
                            view.viewEvent.onNext(Commands.OnError)
                        }, {
                            view.viewEvent.onNext(
                                Commands.OnSuccess(
                                    page = it.list.page,
                                    movieList = it.list.results
                                )
                            )
                        }
                        )
                    }
                    MovieListState.OnLoadState(oldState.state)
                }
                is OnLoadNextPage -> {
                    val stateUpdated = oldState.state.copy(page = oldState.state.page +1)
                    repository.list(page = stateUpdated.page).subscribe { result ->
                        result.fold({
                            view.viewEvent.onNext(Commands.OnError)
                        }, {
                            view.viewEvent.onNext(
                                Commands.OnSuccess(
                                    page = it.list.page,
                                    movieList = it.list.results
                                )
                            )
                        }
                        )
                    }
                    MovieListState.OnLoadState(stateUpdated)
                }
                is Commands.OnError -> {
                    when (oldState.state.page) {
                        1 -> MovieListState.ErrorState(oldState.state)
                        else -> MovieListState.ErrorStateLoadingPage(oldState.state)
                    }

                }

                is Commands.OnSuccess -> MovieListState.SuccessState(
                    oldState.state.copy(
                        movieList = event.movieList,
                        page = event.page
                    )
                )
            }
        }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                data.value = it
            }
        composite.add(observable)
    }

    fun onDetach() {
        composite.clear()
    }

    data class ViewModelState(
        val movieList: List<Movie> = emptyList(),
        val page: Int = 1,
        val totalPages: Int = 500,
        val hasMorePages: Boolean = page < totalPages
    )

    sealed class MovieListState(open val state: ViewModelState) {
        data class InitialState(override val state: ViewModelState) : MovieListState(state)
        data class OnLoadState(override val state: ViewModelState) : MovieListState(state)
        data class ErrorState(override val state: ViewModelState) : MovieListState(state)
        data class ErrorStateLoadingPage(override val state: ViewModelState) : MovieListState(state)
        data class SuccessState(override val state: ViewModelState) : MovieListState(state)
    }

    sealed class Commands {
        object OnLoad : Commands()
        object OnLoadNextPage : Commands()
        object OnError : Commands()
        data class OnSuccess(val page: Int, val movieList: List<Movie>) : Commands()
    }

    interface Target {
        val viewEvent: PublishSubject<Commands>
    }
}