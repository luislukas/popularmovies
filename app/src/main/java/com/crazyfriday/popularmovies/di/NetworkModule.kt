package com.crazyfriday.popularmovies.di

import com.crazyfriday.popularmovies.BuildConfig
import com.crazyfriday.popularmovies.api.MovieApi
import com.crazyfriday.popularmovies.repository.MovieRepository
import com.crazyfriday.popularmovies.viewmodel.MainActivityViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single { provideRetrofit(get()) }
    single { provideOkHttpClient() }
    single { provideMovieApi(get()) }
    factory { provideMovieRepository(get()) }
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit =
    Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

fun provideOkHttpClient(): OkHttpClient = OkHttpClient().newBuilder()
    .addInterceptor(HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    })
    .build()

fun provideMovieApi(retrofit: Retrofit): MovieApi = retrofit.create(MovieApi::class.java)

fun provideMovieRepository(api: MovieApi) = MovieRepository(api, Schedulers.io(), AndroidSchedulers.mainThread())

val viewModelModule = module {
    viewModel { MainActivityViewModel(get()) }
}