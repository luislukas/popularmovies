package com.crazyfriday.popularmovies.model

import com.crazyfriday.popularmovies.model.MovieResult.Movie
import com.google.gson.annotations.SerializedName

data class MovieListResponse(
    val page: Int = 0,
    val total_results: Int = 0,
    val total_pages: Int = 0,
    val results: List<Movie> = emptyList(),
    val success: Boolean = true
)

sealed class MovieResult {
    data class Movie(
        val title: String,
        @SerializedName("poster_path")
        val imageUrl: String,
        @SerializedName("vote_average")
        val votingAverage: Float,
        @SerializedName("release_date")
        val releaseDate: String
    ) : MovieResult()

    object LoadingItem : MovieResult()
    object ErrorLoadingItem : MovieResult()
}
